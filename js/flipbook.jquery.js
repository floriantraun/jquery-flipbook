/*
 *	Author: Florian Abensperg-Traun <flo@floriantraun.at>
 *	Version: 2017-09-21
 *	Website: https://floriantraun.at/
 *
 *	All Rights Reserved.
*/

// Creating the function .flipbook()
$.fn.flipbook = function( options ) {

	// For each element - so the function works on multiple elements on the
	// same page
	return this.each(function () {

		// Define the settings
		var settings = $.extend({
			'pageClass': 'page',
			'activePageClass': 'active',
			'firstPageClass': 'first-page',
			'lastPageClass': 'last-page',
			'leftPageClass': 'page-left',
			'rightPageClass': 'page-right'
	    }, options);

		// Define flipbook variable - avoid errors and confusion when
		// using $(this)
		var flipbook = $(this);

		// Get all pages
		var pages = flipbook.children();
		var pagesLength = pages.length;

		// Define var currentPage if not already set - avoids resetting
		// the variable to zero every time the function is called, also display
		// the first two pages
		if (!currentPage) {
			var currentPage = 0;

			var page = pages.eq(currentPage);
			var nextPage = pages.eq(currentPage +1);

			page.addClass(settings.activePageClass + ' ' + settings.leftPageClass + ' ' + settings.firstPageClass);
			nextPage.addClass(settings.activePageClass + ' ' + settings.rightPageClass);

			if (currentPage == pagesLength-2) {
				nextPage.addClass(settings.lastPageClass);
			}
		}

		// Trigger function when a page is clicked
		$('.' + settings.pageClass).click(function () {
			// Define page variable - avoid errors and confusion when
			// using $(this)
			var page = $(this);

			// Define all page states, so they can be accessed even after
			// all classes have been removed in the next step
			var isActive = page.hasClass(settings.activePageClass);
			var isPageLeft = page.hasClass(settings.leftPageClass);
			var isPageRight = page.hasClass(settings.rightPageClass);
			var isFirstPage = page.hasClass(settings.firstPageClass);
			var isLastPage = page.hasClass(settings.lastPageClass);

			// Remove all state-classes from every page
			pages.each(function () {
				$(this).removeClass(settings.activePageClass);
				$(this).removeClass(settings.leftPageClass);
				$(this).removeClass(settings.rightPageClass);
				$(this).removeClass(settings.firstPageClass);
				$(this).removeClass(settings.lastPageClass);
			});

			// Check if the left page is clicked, and either set state-class
			// 'first-page' or go back to the last 2 pages
			if (isPageLeft) {
				if (currentPage == 0) {
					page.addClass(settings.firstPageClass);
				} else {
					currentPage = currentPage-2;
				}
			}

			// Check if the right page is clicked, and either set state-class
			// 'last-page' or go forward to the next 2 pages
			if (isPageRight) {
				if (currentPage == pagesLength-2) {
					page.addClass(settings.lastPageClass);
				} else {
					currentPage = currentPage+2;
				}
			}

			// Set the state-classes 'active', 'page-left' and 'page-right'
			pages.eq(currentPage).addClass(settings.activePageClass + ' ' + settings.leftPageClass);
			pages.eq(currentPage +1).addClass(settings.activePageClass + ' ' + settings.rightPageClass);
		});
	});
}
